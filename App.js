import { StyleSheet, View, TextInput, Button, FlatList } from 'react-native';
import { useState } from 'react';
import GoalItem from './components/GoalItem';
import GoalInput from './components/GoalInput';

export default function App() {
    const [courseGoals, setCourseGoals] = useState([]);
    const [isAddMode, setIsAddMode] = useState(false);

    const addGoalHandler = (enteredGoal) => {
        setCourseGoals((currentGoals) => [
            ...courseGoals,
            { key: Math.random().toString(), value: enteredGoal },
        ]);
        setIsAddMode(false);
    };

    const deleteHandler = (goalId) => {
        setCourseGoals((currentGoals) => {
            return currentGoals.filter((goal) => {
                return goal.key !== goalId;
            });
        });
    };

    const cancelGoalAddHandler = () => {
        setIsAddMode(false);
    };

    return (
        <View style={styles.screen}>
            <Button title="Add New Goal" onPress={() => setIsAddMode(true)} />
            <GoalInput
                visible={isAddMode}
                onCancel={cancelGoalAddHandler}
                onAddGoal={addGoalHandler}
            />
            <FlatList
                data={courseGoals}
                renderItem={(itemData) => (
                    <GoalItem
                        id={itemData.item.key}
                        onDelete={deleteHandler}
                        title={itemData.item.value}
                    />
                )}
            />
        </View>
    );
}

const styles = StyleSheet.create({
    screen: {
        padding: 70,
    },
});
